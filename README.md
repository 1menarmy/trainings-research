# Welcome in React & Redux training using ES6 features 
Training room 1.6G

## Great React presentations
* https://www.slideshare.net/AndrewHull/react-js-and-why-its-awesome
* https://www.slideshare.net/nikgraf/react-redux-introduction
* https://www.slideshare.net/nikgraf/react-redux-introduction
* https://www.slideshare.net/dreamlab/intro-to-redux-dreamlab-academy-3

## Intial setup
1. Please download and install [NodeJS](https://nodejs.org/en/download/) using LTS (6.10.xxx) version, verify it by going to console and run node --version OR npm --version
2. Install YARN instaed of npm [Yarn](https://yarnpkg.com/lang/en/docs/install)

## Editors
* You can use practically whatever. My favorite one is [Idea / Webstorm](https://www.jetbrains.com/idea/) from IntelliJ
* But there is plenty of awesome IDEs like :
* * [Sublime](https://www.sublimetext.com/) 
* * [VisualStudio code](https://code.visualstudio.com/)
* * [Eclipse](https://eclipse.org/webtools/)
* * [Atom](https://atom.io/)

## Code: 
Go ahead and try:
```
$ git clone https://bitbucket.org/1menarmy/trainings-research.git
```
Go to branch ReactReduxES6Training end go co 4th commit 18046EB "First component totally from scratch"

## Starter / stack setup
* https://docs.npmjs.com/files/package.json
* https://webpack.js.org/
* * https://webpack.github.io/docs/webpack-dev-server.html
* * https://webpack.github.io/docs/loaders.html
* https://babeljs.io/

## Firebase configuration 
```
#!json

const config = {
  apiKey: "AIzaSyDL_qa_mU_kvVj-5rrwesJz8sQ3NldIDic",
  authDomain: "reactreduxes6training.firebaseapp.com",
  databaseURL: "https://reactreduxes6training.firebaseio.com",
  projectId: "reactreduxes6training",
  storageBucket: "reactreduxes6training.appspot.com",
  messagingSenderId: "560456951505",
  email: "", -> Your tieto email
  password:"rrones6training",
};
```

Have fun!